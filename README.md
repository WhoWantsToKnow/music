# Music

### bash script(s) for manipulating music files

----------------------------------------------------------------------------------------

## Acknowledgement

This README brought to you courtesy of the [Acme Readme Corp.][1]

----------------------------------------------------------------------------------------

## Installation

Simply clone the repo to a suitable local directory and, if required, move the scripts
to a folder in your path. One option is to have a 'bin' folder in your home directory
 [which is also added to $PATH][3].

The '~/bin' directory can then have links setup to the script in question which allows
the scripts to be easily invoked.

*NB for the link name it is convenient to drop the '.sh' suffix.*

----------------------------------------------------------------------------------------

## Usage

See individual scripts.

----------------------------------------------------------------------------------------

## Reporting bugs

Please use the [project issue tracker][2] for any bugs or feature suggestions.

----------------------------------------------------------------------------------------

## Contributing

1. [Fork the project][4]
2. Create your feature branch: ***`git checkout -b my-new-feature`***
3. Commit your changes: ***`git commit -am 'Add some feature'`***
4. Push to the branch: ***`git push origin my-new-feature`***
5. [Submit a merge request][5]

Contributions must be licensed under the GNU GPLv3.
The contributor retains the copyright.

----------------------------------------------------------------------------------------

## Credits

----------------------------------------------------------------------------------------

## License
This project is licensed under the GNU General Public License, v3. A copy of this license
is included in the file [LICENSE.md](LICENSE.md).

  [1]: https://gist.github.com/zenorocha/4526327
  [2]: https://gitlab.com/WhowantsToknow/Music/issues
  [3]: https://www.howtogeek.com/658904/how-to-add-a-directory-to-your-path-in-linux/
  [4]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork
  [5]: https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html
