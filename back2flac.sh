#!/bin/bash
set -eE
# ==================================================================================== #
#         FILE: back2flac.sh                                                           #
# ==================================================================================== #
#  DESCRIPTION:  This is a script to take original music CDs and rip them to flac      #
#  files, tag them with information about the album and track using entries in the     #
#  GnuDB CD database plus user additions, rename them using the tag information and    #
#  then copy them to my main music library store.                                      #
#                                                                                      #
#  A lot of the design and logic used in this script has been copied from the most     #
#  excellent 'abcde'(1) script. Which should be considered if a more generic solution  #
#  for organising a music collection is required. Also see the musicbrainz 'picard'(2) #
#  application.                                                                        #
#       1. https://abcde.einval.com/wiki/                                              #
#       2. https://picard.musicbrainz.org/                                             #
#                                                                                      #
#  NB THIS SCRIPT WAS WRITTEN AND DEVELOPED USING BASH 5.1, IT SHOULD BE COMPATIBLE    #
#  == WITH MOST VERSIONS OF BASH 4 BUT EARLIER VERSIONS MAY NOT RUN AS EXPECTED.       #
#                                                                                      #
#  Coding Conventions:                                                                 #
#  -------------------                                                                 #
#  a. constants are in all uppercase (There is a possibility of conflict with an       #
#     environment variable, care must be taken).                                       #
#  b. variables start with a capital letter followed by lower case alphanumerics       #
#     (interstitial upper case letters (CamelCaps) are allowed)                        #
#  c. constants and variables are pre-declared using the 'declare' builtin except      #
#     for the control variables of a 'for...do...done' construct.                      #
#  d. put braces around all variable and constant references e.g.$Var => ${Var}        #
#  e. functions start with 'fn', the remainder of the name follows the same convention #
#     as for variable names.                                                           #
#  f. all functions start with a comment header which describes their purpose and      #
#     usage.                                                                           #
#  g. local constants and variables are declared at the beginning of the function      #
#     definition. (browse function definitions in body of script to see exact format   #
#     and layout used)                                                                 #
#  h. the primary logic is defined in a 'fnMain' function which is the first function  #
#     declared in the script.                                                          #
#  i. all functions are declared before any in-line commands.                          #
#  j. global constants and variables are declared at the end of the script.            #
#  k. the only in-line commands are at the end of the script and are a call the fnMain #
#     function and the final exit statement.                                           #
#                                                                                      #
# ==================================================================================== #
#        USAGE: execute script with no arguments                                       #
#        NEEDS: cd-discid, cdparanoia, curl, flac, metaflac, dos2unix, magick, iconv   #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.0                                                                  #
# DATE CREATED: 08-Aug-2022                                                            #
# LAST CHANGED: 12-Oct-2022 16:57                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnMain() {
# ------------------------------------------------------------------------------------ #
# holds the primary processing logic for the script.                                   #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${FALSE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # append a run start marker to the log file
    fnLog "$(fnCopies "#" 80)"
    fnLog "${SCRIPTNAME} : Run started"
    fnLog "$(fnCopies "#" 80)"
    #
    if fnGetDiscID; then                            # only continue if discid obtained
        if fnStorageLayoutOK; then
            if fnGnuDBServerOK; then
                if fnGetGnuDBEntriesList; then
                    if fnGetGnuDBEntries; then       # invokes external editor
                        if fnParseGnuDBEntries; then
                            if fnGetCoverImage; then
                                if fnGetWavFilesFromCD; then
                                    if fnEncodeWavToFlac; then
                                        if fnFillTagFields; then
                                            if fnRenameTracks; then
                                                if fnCopyToLibrary; then
                                                    eject
                                                    Result=${TRUE}
                                                fi
                                            fi
                                        fi
                                    fi
                                fi
                            fi
                        fi
                    fi
                fi
            fi
        fi
    fi
    if (( Result == FALSE )); then
        fnEcho "Errors have occurred, see ${Log} for more information"
    fi
    # append a run end marker to the log file
    fnLog "$(fnCopies "#" 80)"
    fnLog "${SCRIPTNAME} : Run complete"
    fnLog "$(fnCopies "#" 80)"
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}

# ------------------------------------------------------------------------------------ #
function fnMunge() {
# ------------------------------------------------------------------------------------ #
# Scans the supplied argument string replacing specified characters with others or     #
# removing some completely.                                                            #
#                                                                                      #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Source="${*}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Source="${Source//&/and}"           # expand all ampersands to 'and'
    Source="${Source//\\}"              # delete all back slashes
    Source="${Source//.}"               # delete all full stops
    Source="${Source//\'}"              # delete all apostrophes
    Source="${Source//\"}"              # delete all quotes
    Source="${Source//( /(}"            # replace '( ' with '('
    Source="${Source// )/)}"            # replace ' )' with ')'
    Source="${Source//  / }"            # replace double space with single space
    # see https://www.educba.com/bash-trim-string/ for following
    Source="${Source#"${Source%%[![:space:]]*}"}" # remove leading whitespace characters
    Source="${Source%"${Source##*[![:space:]]}"}" # remove trailing whitespace characters
    Source="${Source// /_}"             # replace all spaces with underscores
    Source=$(${ICONV} -f UTF-8 -t ASCII//TRANSLIT <<<"${Source}") # remove diacritics
    printf "${Source}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnCopies() {
# ------------------------------------------------------------------------------------ #
# Repeats a string value a specified number of times.                                  #
#                                                                                      #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
#     2) integer - number of times string is to be repeated.                           #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    C="${1}"
    declare -i N=${2}
    declare    S="$(printf "%*s" "${N}" "")"
    declare    T="${S// /${C}}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "${T}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnEcho() {
# ------------------------------------------------------------------------------------ #
# Provide a replacement for the 'echo' command using the printf builtin. Output is     #
# prefixed with a tab character.                                                       #
#                                                                                      #
# Arguments: one or more strings to be output to stdout                                #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "\t> %b\n" "${@}"
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
# shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
function fnLog() {
# ------------------------------------------------------------------------------------ #
# The argument string is written to the log file but prefixed with the date and time,  #
# followed by the current function context.                                            #
#                                                                                      #
# Arguments: one or more strings to be output to stdout                                #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Context=""
    declare    Logtext="${*}"
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Context="${FUNCNAME[1]}"  # get calling function, '0' would be this function i.e. fnLog
    Logtext="${Logtext//[$'\n\r\t']}"   # remove any EOL control characters
    printf "%(%d-%b-%y %H:%M:%S)T : %21s] %s %s\n"  ${NOW} "${Context}" "${Logtext}" >> "${Log}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnAppendToFile() {
# ------------------------------------------------------------------------------------ #
# Append a line to a specified file.                                                   #
#                                                                                      #
# Arguments: 1:    full path to file                                                   #
#            2...: string to be appended to the file                                   #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    File="${1}"
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    shift
    printf "%b" "${@}" >> "${File}"
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetDiscID() {
# ------------------------------------------------------------------------------------ #
# Runs 'cd-discid' to get information about the CD to be copied to flac files. Also    #
# checks that as CD device is present and that a CD is present.                        #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # regex strings to match against possible responses
    declare -r NODRIVE="^.*: open: No such file or directory"
    declare -r NODISC="^.*: CDROMREADTOCHDR: No medium found"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "Examining CD to determine discid..."
    # process command output line by line
    while IFS= read -r Line; do
        if [[ ${Line} =~ ${NODRIVE} ]]; then
            fnLog "FATAL: CD drive not found."
            Result=${FALSE}
        elif [[ ${Line} =~ ${NODISC} ]]; then
            fnLog "ERROR: No audio disc found in drive."
            Result=${FALSE}
        elif (( Result == TRUE )); then
            # response should be in form;
            # discid no.tracks offset1 offset2...offsetN total-time(secs)
            fnLog "${CDDISCID}=${Line}"
            read -r -a Field <<< "${Line}" # split line into component parts
            DiscID="${Field[0]}"
            NTracks="${Field[1]}"
            Time="${Field[-1]}"         # -1 is last element in array
            for (( F=2; F<=(NTracks+1); F++ ));do
                Offset+=("${Field[${F}]}")
            done
            fnLog "found audio CD with ${NTracks} tracks, DiscID=${DiscID}"
            fnEcho "found audio CD with ${NTracks} tracks, DiscID=${DiscID}"
        fi
    done < <(${CDDISCID} 2>&1)
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnStorageLayoutOK() {
# ------------------------------------------------------------------------------------ #
# Ensure that the directory structure needed for the processing of the CD exists.      #
#                                                                                      #
# Storage layout:                                                                      #
#                    WORK                                                              #
#                     |                                                                #
#                     +--WorkDir (Discid)                                              #
#                           | bulk.xmcd                                                #
#                           | [discid].xmcd                                            #
#                           |                                                          #
#                           +--WavDir (wavfiles)                                       #
#                           |                                                          #
#                           +--FlacDir (flacs)                                         #
#                           |                                                          #
#                           +--TrackDir (tracks)                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${FALSE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # check root directory exists
    if [[ -d "${WORK}" ]]; then
        fnLog "Root working directory is ${WORK}"
        # check for a working directory under the root using the discid
        WorkDir="${WORK}${DiscID}/"
        if fnMakeWorkDir "${WorkDir}"; then
            # move run log to working directory
            if fnMoveLog; then
                WavDir="${WorkDir}wavfiles/"
                FlacDir="${WorkDir}flacs/"
                TrackDir="${WorkDir}tracks/"
                if fnMakeWorkDir "${WavDir}"; then
                    if fnMakeWorkDir "${FlacDir}"; then
                        CoverArt="${FlacDir}cover.jpg"
                        BulkXmcd="${WorkDir}bulk.xmcd"
                        XmcdFile="${WorkDir}${DiscID}.xmcd"
                        if fnMakeWorkDir "${TrackDir}"; then
                            fnEcho "set up working structure under ${WorkDir}"
                            Result=${TRUE}
                        fi
                    fi
                fi
            fi
        fi
    else
        fnEcho "Cannot find ${WORK}"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnMakeWorkDir() {
# ------------------------------------------------------------------------------------ #
# Check if a directory exists within the working directory and, if it doesn't, create  #
# it.                                                                                  #
#                                                                                      #
# Arguments: directory path                                                            #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DIRECTORY="${1}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # check if target directory already exists
    if [[ -d "${DIRECTORY}" ]]; then
        fnLog "directory ${DIRECTORY/#${WORK}/...} found, assuming rerun."
    else
        # create the target directory
        if mkdir -p "${DIRECTORY}"; then
            fnLog "Directory \"${DIRECTORY/#${WORK}/...}\" successfully created."
        else
            fnLog "Failed to create ${DIRECTORY/#${WORK}/...}"
            Result=${FALSE}
        fi
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnMoveLog() {
# ------------------------------------------------------------------------------------ #
# Move the initial log file under root to the working directory and set the $Log       #
# variable to point to it.                                                             #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r NEWLOG="${WorkDir}${Log##*/}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${FALSE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if cp -a "${Log}" "${NEWLOG}"; then
        # copy worked so delete initial version
        if rm "${Log}"; then
            # delete of initial log file worked so reset $Log variable
            Log="${NEWLOG}"
            fnLog "log file moved to ${WorkDir}"
            fnEcho "Logging to ${Log}"
            Result=${TRUE}
        else
            fnLog "failed to delete ${Log}, \$Log variable not reset"
        fi
    else
        fnLog "copy of ${Log} to ${NEWLOG} failed"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnMakeLibraryDir() {
# ------------------------------------------------------------------------------------ #
# Check if a directory exists within the music library directory and, if it doesn't,   #
# create it.                                                                           #
#                                                                                      #
# Arguments: directory path                                                            #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DIRECTORY="${1}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # check if target directory already exists
    if [[ -d "${DIRECTORY}" ]]; then
        fnLog "directory ${DIRECTORY/#${LIBRARY}/...} found, assuming rerun."
    else
        # create the target directory
        if mkdir -p "${DIRECTORY}"; then
            fnLog "Directory \"${DIRECTORY/#${LIBRARY}/...}\" successfully created."
        else
            fnLog "Failed to create ${DIRECTORY/#${LIBRARY}/...}"
            Result=${FALSE}
        fi
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGnuDBServerOK() {
# ------------------------------------------------------------------------------------ #
# Query the GnuDB server status. Return 'true' if it's available.                       #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  STAT="${GNUDBURL}?cmd=stat&${HELLO}"
    declare -r  OK="210"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a Response
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "Checking if GnuDB server is available"
    # send http to GnuDB to query status
    fnLog "sending \"${HTTP} ${STAT}\" to GnuDB"
    # get response in an array
    if mapfile -t Response < <(${HTTP} "${STAT}"); then
        # process command output line by line but only look at 1st
        for (( Line=0; Line<${#Response[@]}; Line++ )); do
            fnLog "${Response[${Line}]}"
            if (( Line == 0 )); then
                if [[ "${Response[${Line}]:0:3}" != "${OK}" ]]; then
                    fnLog "FAIL : GnuDB server not available"
                    Result=${FALSE}
                    break
                fi
            fi
        done
        fnLog "GnuDB server check OK"
        fnEcho "GnuDB server check OK"
    else
        fnLog "GnuDB server check failed"
        Result=${FALSE}
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetGnuDBEntriesList() {
# ------------------------------------------------------------------------------------ #
# Using the information returned by 'fnGetDiscID', query GnuDB and get a list of       #
# matching entries present in the database. Save the categories so the full XMCD       #
# entries can be retrieved later.                                                      #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r CMD="cmd=CDDB+query"
    declare -r OFFSETS="$(printf "+%s" "${Offset[@]}")"
    declare -r QUERY="${GNUDBURL}?${CMD}+${DiscID}+${NTracks}${OFFSETS}+${Time}&${HELLO}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -a Response                 # stores response lines from http call
    declare -i Hits=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "checking if disc has entries on GnuDB server..."
    # send http to GnuDB to query status
    fnLog "sending \"${HTTP} ${QUERY}\" to GnuDB"
    # get response in an array
    if mapfile -t Response < <(${HTTP} "${QUERY}"); then
        # process command output line by line
        for (( Line=0; Line<${#Response[@]}; Line++ )); do
            fnLog "${Response[${Line}]}"
            if (( Line == 0 )); then
                # 1st line of o/p has response status
                case "${Response[${Line}]}" in
                    200*) # Found exact match
                        fnSaveCategory "${Response[${Line}]}"
                        ;;
                    202*) # No match found, will need to generate a blank template
                        NoMatchFound=${TRUE}
                        fnLog "Query failed, no entries found. Will generate blank template."
                        break
                        ;;
                    210*) # Found exact matches, list follows (until terminating marker)
                        continue
                        ;;
                    211*) # Found inexact matches, list follows (until terminating marker)
                        continue
                        ;;
                    403*) # Database entry is corrupt
                        Result=${FALSE}
                        fnLog "Query failed, database entry is corrupt"
                        break
                        ;;
                       *) # unknown response
                        Result=${FALSE}
                        fnLog "Query failed, unexpected response"
                        break
                        ;;
                esac
            else
                # only save category if not end of list (length < 2)
                if [[ "${#Response[${Line}]}" -gt 2 ]]; then
                    fnSaveCategory "${Response[${Line}]}"
                fi
            fi
        done
        if (( NoMatchFound != TRUE )); then
            Hits=${#Category[@]}
            if (( Hits > 0 )); then
                fnLog "GnuDB entries found. Returned ${Hits} categories."
                fnEcho "GnuDB entries found. Returned ${Hits} categories."
            fi
        fi
    else
        fnLog "ERROR: GnuDB query call failed"
        Result=${FALSE}
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnSaveCategory() {
# ------------------------------------------------------------------------------------ #
# Save the category from the response line and set the various artists flag if         #
# needed. Response lines can be of 2 types depending on the status returned to the     #
# calling function.                                                                    #
#     1) status lines where there is a single exact match:                             #
#             200 'category' 'discid' 'album artist' / 'album title'                   #
#     2) status lines where there are multiple matches:                                #
#             'category' 'discid' 'album artist' / 'album title'                       #
#                                                                                      #
# Arguments: text of http response line                                                #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r RESPONSELINE="${*}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    read -r -a Field <<< "${RESPONSELINE}"
    # if single exact match line shift array left by one
    if [[ "${Field[0]}" == "200" ]]; then
        Field=("${Field[@]:1}")
    fi
    # set the various artists flag if required
    if [[ "${Field[2],,}" == "${VARIOUS}"* ]]; then
        VariousArtists=${TRUE}
    fi
    # get the category from 1st word of response line
    Category+=("${Field[0]}")
    fnLog "${#Category[@]} ${Field[0]}"
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetGnuDBEntries() {
# ------------------------------------------------------------------------------------ #
# Using the categories retrieved by fnGetGnuDBEntriesList this function will obtain    #
# the XMCD files for each category and create a bulk XMCD file for the user to edit to #
# a single valid entry. It first checks to see if a final XMCD already exists and, if  #
# so, exits without re-reading the database entries.                                   #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none (but it creates a bulk XMD file)                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r FOUND="210"
    declare -r READ1="${GNUDBURL}?cmd=CDDB+read+"
    declare -r READ2="+${DiscID}&${HELLO}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i First
    declare    Read
    declare    Keyword
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "checking for existing xmcd file..."
    if [[ -f "${XmcdFile}" ]]; then
        fnLog "${XmcdFile/#${WorkDir}/...} exists, using this version."
    else
        # if required generate a blank XMCD template file
        if (( NoMatchFound == TRUE )); then
            fnGenerateNewXMCD
        else
            fnLog "none found so reading disc entries from GnuDB server."
            rm -f "${BulkXmcd}"                 # clear out any previous file
            # query database for each category returned by fnGetGnuDBEntriesList
            for Cat in "${Category[@]}"; do
                Read="${READ1}${Cat}${READ2}"
                First=${TRUE}
                # process command output line by line
                while IFS= read -r Line; do
                    # 1st line of o/p is cmd status
                    if (( First == TRUE )); then
                        First=${FALSE}
                        # display status line
                        fnLog "${Line}"
                        # break out if status code not ok
                        if [[ "${Line:0:3}" != "${FOUND}" ]]; then
                            break
                        fi
                        # prefix entry with status line
                        fnAppendToFile "${BulkXmcd}" "# Status: ${Line}\n"
                    else
                        # add all but 1st line of output to file
                        # ignore short lines < 2 (blank comments & EOL marker)
                        if [[ "${#Line}" -gt 2 ]]; then
                            # split string around "=" character (see 'bash parameter
                            # substitution')
                            Keyword="${Line%%=*}"   # allow for '=' in value string
                            # if first EXTT line insert general EXTD vorbis comment
                            # templates
                            if [[ "${Keyword}" == "EXTT0" ]]; then
                                for Key in "${VORALBUM[@]}"; do
                                    fnAppendToFile "${BulkXmcd}" "EXTD=VORBIS[${Key}=]\n"
                                done
                                # for a classical album that's not for various artists
                                # insert classical EXTD vorbis comment templates
                                if [[ " ${CLASSICAL[*],,} " =~ \ ${Cat,,}\  ]] && \
                                   (( VariousArtists == FALSE )); then
                                    for Key in "${VORTRACK[@]}"; do
                                        fnAppendToFile "${BulkXmcd}" "EXTD=VORBIS[${Key}=]\n"
                                    done
                                fi
                            fi
                            # add the line being processed
                            fnAppendToFile "${BulkXmcd}" "${Line}"
                            # for a classical album that's for various artists add extra
                            # vorbis comments (,, = lowercase)
                            if [[ " ${CLASSICAL[*],,} " =~ \ ${Cat,,}\  ]] && \
                               (( VariousArtists == TRUE )); then
                                # after every EXTT line add vorbis comment templates
                                if [[ "${Keyword:0:4}" == "EXTT" ]]; then
                                    for Key in "${VORTRACK[@]}"; do
                                        fnAppendToFile "${BulkXmcd}" "${Keyword}=VORBIS[${Key}=]\n"
                                    done
                                fi
                            fi
                        fi
                    fi
                done < <(${HTTP} "${Read}")
            done
            # check that there is a consolidated xmcd file to be processed, make sure
            # the line endings are unix style, copy the bulk file to a working xmcd
            # and invoke the editor so any corrections/additional information can
            # be made/added
            if [[ -f "${BulkXmcd}" ]]; then
                fnLog "Consolidated xmcd file created..."
                fnLog "Ensuring line endings are in unix format..."
                "${FIXEOL}" "${BulkXmcd}" >/dev/null 2>&1
                "${FIXEOL}" -c mac "${BulkXmcd}" >/dev/null 2>&1
                if cp "${BulkXmcd}" "${XmcdFile}"; then
                    fnLog "${BulkXmcd/#${WorkDir}/...} copied to ${XmcdFile/#${WorkDir}/...}, editor invoked... "
                    # shellcheck disable=SC2154 # EDITOR is an environment variable
                    ${EDITOR} "${NEWINSTANCE}" "${XmcdFile}"  >/dev/null 2>&1 &
                    wait $!
                    fnLog "...finished."
                else
                    fnLog "...failed to create ${XmcdFile/#${WorkDir}/...}"
                    Result=${FALSE}
                fi
            else
                fnLog "...failed to create ${BulkXmcd/#${WorkDir}/...}"
                Result=${FALSE}
            fi
        fi
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGenerateNewXMCD() {
# ------------------------------------------------------------------------------------ #
# Generate an empty xmcd file using data from fnGetDiscID and invoke editor so album   #
# and track titles can be added.                                                       #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # reset no match found flag
    NoMatchFound=${FALSE}
    # create an empty XMCD file using the data obtained from fnGetDiscID
    cat <<ENDXMCD >"${XmcdFile}"
# xmcd
#
# Track frame offsets:
ENDXMCD
    # generate track frame offset list
    for TFO in "${Offset[@]}"; do
        fnAppendToFile "${XmcdFile}" "# ${TFO}"
    done
    # generate further blank XMCD entries
    cat <<ENDXMCD >>"${XmcdFile}"
#
# Disc length: ${Time} seconds
#
# Revision: 0
# Submitted via: ${SCRIPTNAME} ${VERSION}
#
DISCID=${DiscID}
DTITLE=
DYEAR=
DGENRE=
ENDXMCD
    # generate blank track title entries - one per track
    for (( TNo=0; TNo<NTracks; TNo++ )); do
        fnAppendToFile "${XmcdFile}" "TTITLE${TNo}="
    done
    for Key in "${VORALBUM[@]}"; do
        fnAppendToFile "${XmcdFile}" "EXTD=VORBIS[${Key}=]\n"
    done
    # generate blank extended track data entries - one per track
    for (( TNo=0; TNo<NTracks; TNo++ )); do
        for Key in "${VORTRACK[@]}"; do
            fnAppendToFile "${XmcdFile}" "EXTT${TNo}=VORBIS[${Key}=]\n"
        done
    done
    fnAppendToFile "${XmcdFile}" "PLAYORDER="
    #
    fnLog "generated empty xmcd file as ${XmcdFile/#${WorkDir}/...}, editor invoked... "
    ${EDITOR} "${NEWINSTANCE}" "${XmcdFile}"  >/dev/null 2>&1 &
    wait $!
    fnLog "...finished."
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnParseGnuDBEntries() {
# ------------------------------------------------------------------------------------ #
# Processes an XMCD file to extract the tag values for later addition to the flac      #
# files created from the CD.                                                           #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare    Keyword
    declare    Value
    declare    Vorbis
    declare -i Trackno
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "Processing the xmcd file : ${XmcdFile}"
    while read -r Line; do
        # ignore comment lines
        if [[ ${Line:0:1} == "#" ]]; then
            continue
        fi
        # split string around "=" character
        # google 'bash parameter substitution' to see why this works
        Keyword="${Line%%=*}"           # allow for '=' in value string
        Value="${Line#*=}"
        case "${Keyword}" in
            # entries relating to entire album
            DTITLE)
                Albumartist="${Value% / *}"
                fnLog "Albumartist=${Albumartist}"
                Album="${Value#* / }"
                fnLog "Album=${Album}"
                ;;
            DYEAR)
                Date="${Value}"
                fnLog "Date=${Date}"
                ;;
            DGENRE)
                Genre="${Value}"
                fnLog "Genre=${Genre}"
                ;;
            EXTD)
                # save vorbis comment values
                if [[ "${Value:0:6}" == "VORBIS" ]]; then
                    # extract the 'keyword=value' between square brackets
                    Vorbis="${Value#*[}"  # delete '[' and all before it
                    Vorbis="${Vorbis%]*}" # delete ']' and all after it
                    # get vorbis keyword and value
                    Keyword="${Vorbis%=*}"
                    Value="${Vorbis#*=}"
                    case "${Keyword}" in
                        ALBUMARTISTSORT)
                            Albumartistsort="${Value}"
                            fnLog "Albumartistsort=${Albumartistsort}"
                            ;;
                        DISCNUMBER)
                            Discnumber="${Value}"
                            fnLog "Discnumber=${Discnumber}"
                            ;;
                        TOTALDISCS)
                            Totaldiscs="${Value}"
                            fnLog "Totaldiscs=${Totaldiscs}"
                            ;;
                        TOTALTRACKS)
                            Totaltracks="${Value}"
                            fnLog "Totaltracks=${Totaltracks}"
                            ;;
                        COMPOSER)
                            Albumcomposer="${Value}"
                            fnLog "Composer=${Albumcomposer}"
                            ;;
                        COMPOSERSORT)
                            Albumcomposersort="${Value}"
                            fnLog "ComposerSort=${Albumcomposersort}"
                            ;;
                        CONDUCTOR)
                            Albumconductor="${Value}"
                            fnLog "Conductor=${Albumconductor}"
                            ;;
                        *)
                            ;;
                    esac
                else
                    Description+="${Value}"
                    fnLog "Description=${Description}"
                fi
                ;;
            # entries relating to individual tracks
            TTITLE*)
                # derive the the track no. from the track index (zero base)
                Trackno=10#${Keyword//[^0-9]/} # delete all characters that are not digits
                (( Trackno++ ))
                # test for ' / ' - i.e. artist / title
                if [[ "${Value}" =~ " / " ]]; then
                    Artist[${Trackno}]="${Value% / *}" # everything before ' / '
                    fnLog "Artist[${Trackno}]=${Artist[${Trackno}]}"
                    Title[${Trackno}]="${Value#* / }"  # everything after ' / '
                else
                    Title[${Trackno}]="${Value}"
                fi
                fnLog "Title[${Trackno}]=${Title[${Trackno}]}"
                ;;
            EXTT*)
                # derive the the track no. from the track index (zero base)
                Trackno=10#${Keyword//[^0-9]/} # delete all characters that are not digits
                (( Trackno++ ))
                if [[ "${Value:0:6}" == "VORBIS" ]]; then
                    # extract the 'keyword=value' between square brackets
                    Vorbis="${Value#*[}"  # delete '[' and all before it
                    Vorbis="${Vorbis%]*}" # delete ']' and all after it
                    # get vorbis keyword and value
                    Keyword="${Vorbis%=*}"
                    Value="${Vorbis#*=}"
                    case "${Keyword}" in
                        COMPOSER)
                            Composer[${Trackno}]="${Value}"
                            fnLog "Composer[${Trackno}]=${Composer[${Trackno}]}"
                            ;;
                        CONDUCTOR)
                            Conductor[${Trackno}]="${Value}"
                            fnLog "Conductor[${Trackno}]=${Conductor[${Trackno}]}"
                            ;;
                        *)
                            ;;
                    esac
                else
                    Soloist[${Trackno}]="${Value}"
                    fnLog "Soloist[${Trackno}]=${Soloist[${Trackno}]}"
                fi
                ;;
            # ignore everything else
            *)
                continue
                ;;
        esac
    done <"${XmcdFile}"
    if (( NTracks != Totaltracks )); then
        fnLog "Total tracks reported by cd-discid ${NTracks} not same as xmcd ${Totaltracks}"
        Result=${FALSE}
    else
        fnLog "extracted tag values from xmcd file."
        fnEcho "extracted tag values from xmcd file."
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetCoverImage() {
# ------------------------------------------------------------------------------------ #
# Searches for a cover artwork file in the specified directory. The filename is        #
# expected to be in the format "ALBUMARTIST-ALBUM.jpg|png|...", with spaces replaced   #
# by underscores. If there are multiple files found the function will take the first   #
# occurrence. The file is then copied to the flac directory with the name 'cover.jpg'. #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r ARTDIR="/home/swillber/Music/H10_Files/Artwork/"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    ArtFile=""
    declare -i FileCount=0
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # construct artwork file names (replace spaces by underscore)
    # extension could be jpg, png, etc so use generic search
    ArtFile="${Albumartist// /_}-${Album// /_}.*"
    fnLog "looking for album artwork for ${ArtFile}"
    # count number of matches
    FileCount=$(${FIND} "${ARTDIR}" -name "${ArtFile}" -printf "%f\n" | wc -l)
    # if found copy to flac directory as ${CoverArt}
    if (( FileCount > 0 )); then
        ArtFile="${ARTDIR}${ArtFile}"
        if "${COPYART}" "${ArtFile}" "${CoverArt}"; then
            fnLog "copied ${ArtFile} to ${CoverArt/#${WorkDir}/...}"
            fnEcho "copied ${ArtFile} to ${CoverArt/#${WorkDir}/...}"
        else
            fnLog "failed to copy ${ArtFile} to ${CoverArt/#${WorkDir}/...}"
            Result=${FALSE}
        fi
    else
        fnLog "no artwork could be located."
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetWavFilesFromCD() {
# ------------------------------------------------------------------------------------ #
# Rip all tracks from the current CD which have not already been ripped (i.e. exist as #
# raw wav or flac files) as wav files. Only the current working directory structure is #
# examined for existing files.                                                         #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  WAVPATTERN="${FILEPATTERN}wav"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i Ripped=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "Ripping all tracks to ${WavDir} wav files..."
    # rip each track to wav if not already ripped
    for (( Track=1; Track<=NTracks; Track++ )); do
        # build the flac file specification for this track
        FlacFile="${FlacDir}$(printf "${FLACPATTERN}" "${Track}")"
        # skip if the flac file exists
        if [[ -f "${FlacFile}" ]]; then
            fnLog "${FlacFile/#${WorkDir}/...} exists, skipping rip for this file."
        else
            # build the wav file specification for this track
            WavFile="${WavDir}$(printf "${WAVPATTERN}" "${Track}")"
            # skip if wav file exists
            if [[ -f "${WavFile}" ]]; then
                fnLog "${WavFile/#${WorkDir}/...} exists, skipping rip for this file."
            else
                fnLog "${WavFile/#${WorkDir}/...} not found, ripping..."
                # no flac or wav for this track so rip to wav
                fnEcho "Ripping track #${Track} ${Artist[${Track}]}-${Title[${Track}]}"
                if "${RIPPER}" -B "${Track}-${Track}" "${WavDir}"; then
                    # clear the ripper output and move up to last message
                    for (( N=1; N<=14; N++ )); do
                        printf "${RLFCLR}"
                    done
                    (( Ripped++ ))
                    Result=${TRUE}
                else
                    fnLog "...errors occurred during rip."
                    Result=${FALSE}
                fi
            fi
        fi
    done
    if (( Result == TRUE )); then
        fnEcho "ripped ${Ripped} tracks to wav files"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnEncodeWavToFlac() {
# ------------------------------------------------------------------------------------ #
# Encode all the wav files in the working directory to flac. Skip any which have       #
# already been converted.                                                              #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # get sorted list of wav file names in wav directory into an array
    # shellcheck disable=SC2207 # Prefer mapfile or read -a to split command output.
    declare -ar WAVFILE=($(find "${WavDir}" -name "*.wav" -printf "%f\n" | sort))
    declare -r  STARTMSG="Encoding ${#WAVFILE[@]} wav files to flac..."
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i Encoded=0
    declare    Wavfile=""
    declare    Flacfile=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "${STARTMSG}"
    fnEcho "${STARTMSG}"
    for Wavfile in "${WAVFILE[@]}"; do
        WavFile="${WavDir}${Wavfile}"
        # get flac version of wav file
        Flacfile="${Wavfile/%wav/flac}"
        # skip if the flac file exists
        FlacFile="${FlacDir}${Flacfile}"
        if [[ -f "${FlacFile}" ]]; then
            fnLog "${FlacFile/#${WorkDir}/...} exists, skipping flac encode for this file."
        else
            if "${ENCODE}" -Vs --best --replay-gain -o "${FlacFile}" "${WavFile}"; then
                fnEcho ".../${Wavfile} encoded as .../${Flacfile}${RLF}"
                fnLog "${WavFile/#${WorkDir}/...} encoded as ${FlacFile/#${WorkDir}/...}"
                (( Encoded++ ))
            else
                Result=${FALSE}
            fi
        fi
    done
    if (( Result == TRUE )); then
        fnEcho "encoded ${Encoded} wav files to flac"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnFillTagFields() {
# ------------------------------------------------------------------------------------ #
# Use the tag data from the xmcd file and set the vorbis tags in the flac files in     #
# the working directory.                                                               #
#                                                                                      #
# Vorbis tag names and descriptions (not exhaustive);                                  #
#       ALBUM               Album Title                                                #
#       ALBUMARTIST         Album Artist                                               #
#       ALBUMARTISTSORT     Sort Album Artist                                          #
#       ARTIST              Track Artist                                               #
#       Artistsort          Sort Track Artist                                          #
#       COMPOSER            Composer                                                   #
#       COMPOSERSORT        Composer Sort-Order                                        #
#       CONDUCTOR           Conductor                                                  #
#       COPYRIGHT           Copyright                                                  #
#       DISCNUMBER          Disc#                                                      #
#       GENRE               Genre                                                      #
#       LANGUAGE            Language                                                   #
#       ORIGYEAR            Original Year                                              #
#       TITLE               Track Title                                                #
#       TITLESORT           Title Sort-Order                                           #
#       TOTALDISCS          Disc Count                                                 #
#       TOTALTRACKS         Track Count                                                #
#       TRACKNUMBER         Track#                                                     #
#       DATE                Album Year (Treated the same as YEAR)                      #
#       YEAR                Album Year (Treated the same as DATE)                      #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # get sorted list of flac file names in flac directory into an array
    # shellcheck disable=SC2207 # Prefer mapfile or read -a to split command output.
    declare -ar FLACFILE=($(find "${FlacDir}" -name "*.flac" -printf "%f\n" | sort))
    declare -r  STARTMSG="Setting tags for ${#FLACFILE[@]} flac files..."
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i Track=0
    declare    Flacfile=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "${STARTMSG}"
    fnEcho "${STARTMSG}"
    # set the per track tags
    for Flacfile in "${FLACFILE[@]}"; do
        fnEcho "setting tags for .../${Flacfile}${RLF}"
        # build the full flac file specification for this file
        FlacFile="${FlacDir}${Flacfile}"
        fnLog "Setting tags for ...${Flacfile}"
        # set the album wide tags (i.e. applies to all tracks)
        fnSetTagValue "ALBUM" "${Album}" "${FlacFile}"
        fnSetTagValue "ARTIST" "${Albumartist}" "${FlacFile}"
        fnSetTagValue "COMPOSER" "${Albumcomposer}" "${FlacFile}"
        fnSetTagValue "CONDUCTOR" "${Albumconductor}" "${FlacFile}"
        fnSetTagValue "DESCRIPTION" "${Description}" "${FlacFile}"
        fnSetTagValue "DISCNUMBER" "${Discnumber}/${Totaldiscs}" "${FlacFile}"
        fnSetTagValue "GENRE" "${Genre}" "${FlacFile}"
        fnSetTagValue "DATE" "${Date}" "${FlacFile}"
        fnSetTagValue "TOTALDISCS" "${Totaldiscs}" "${FlacFile}"
        fnSetTagValue "TOTALTRACKS" "${Totaltracks}" "${FlacFile}"
        # get track number from filename
        Track=10#${Flacfile//[^0-9]/} # delete all characters that are not digits
        # set track specific tag values if not set use albumvalues
        fnSetTagValue "ARTIST" "${Artist[Track]}" "${FlacFile}"
        fnSetTagValue "COMPOSER" "${Composer[Track]}" "${FlacFile}"
        fnSetTagValue "COMMENT" "${Soloist[Track]}" "${FlacFile}"
        fnSetTagValue "CONDUCTOR" "${Conductor[Track]}" "${FlacFile}"
        fnSetTagValue "TITLE" "${Title[Track]}" "${FlacFile}"
        fnSetTagValue "TRACKNUMBER" "${Track}/${Totaltracks}" "${FlacFile}"
        # if there's artwork add it to the track file
        if [[ -f "${CoverArt}" ]]; then
            # clear any existing picture tags, break out if it fails
            if "${TAGGER}" --remove           \
                           --block-type=PICTURE "${FlacFile}"; then
                if ! "${TAGGER}" --import-picture-from="${CoverArt}" \
                                                "${FlacFile}"; then
                    fnLog "### failed to tag ${FlacFile/#${WorkDir}/...} with cover art. ###"
                    Result=${FALSE}
                    break
                fi
            else
                fnLog "failed to clear picture tag(s)."
                Result=${FALSE}
                break
            fi
        fi
    done
    if (( Result == TRUE )); then
        fnEcho "set tag values for each flac file"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnSetTagValue() {
# ------------------------------------------------------------------------------------ #
# Fill an individual vorbis tag for a flac file first removing any existing with same  #                                       #
# name.                                                                                #
#                                                                                      #
# Arguments: 1: the vorbis tag name of the tag to be filled                            #
#            2: the value that the tag should be set to                                #
#            3: full file specification of flac file whose tag is to be set            #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r TAG="${1}"
    declare -r VALUE="${2}"
    declare -r FILE="${3}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # only set tag if value is not a zero length string
    if (( ${#VALUE} > 0 )); then
        # remove any existing tag of same name
        if "${TAGGER}" --remove-tag="${TAG}" "${FILE}"; then
            # and set new tag value
            if ! "${TAGGER}" --set-tag="${TAG}=${VALUE}" "${FILE}"; then
                # return error if tag setting fails
                fnLog "failed setting ${TAG}=${VALUE} for ${FILE/#${WorkDir}/...}"
                Result=${FALSE}
            fi
        else
            # return error if tag removal fails
            fnLog "failed removing ${TAG} from ${FILE/#${WorkDir}/...}"
            Result=${FALSE}
        fi
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnRenameTracks() {
# ------------------------------------------------------------------------------------ #
# Copy all the flac files in the flac directory to a separate 'rename' directory, in   #
# the process rename the file to reflect the tag values so the filename better         #
# reflects the artist, album and track contents.                                       #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # shellcheck disable=SC2207 # Prefer mapfile or read -a to split command output.
    declare -ar FLACFILE=($(find "${FlacDir}" -name "*.flac" -printf "%f\n" | sort))
    declare -r  STARTMSG="Setting tags for ${#FLACFILE[@]} flac files..."
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i Track=0
    declare    Flacfile=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "Copying and renaming ${#FLACFILE[@]} flac files..."
    for Flacfile in "${FLACFILE[@]}"; do
        # build flac filename
        FlacFile="${FlacDir}${Flacfile}"
        # get track number from filename
        Track=10#${Flacfile//[^0-9]/} # delete all characters that are not digits
        # build the full filename reflecting file tags
        # is genre in the classical list
        if [[ " ${CLASSICAL[*],,} " =~ \ ${Genre,,}\  ]]; then
            if (( VariousArtists == TRUE )); then
                # track name for multiple composer/artist CD
                # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
                TrackName="${Album}-$(printf "%02u-%02u" ${Discnumber} ${Track})"
                TrackName+="-${Composer[${Track}]}-${Title[${Track}]}-${Artist[${Track}]}"
                # following items may be blank
                if [[ -n "${Conductor[${Track}]}" ]]; then
                    TrackName+="-${Conductor[${Track}]}"
                fi
                if [[ -n "${Soloist[${Track}]}" ]]; then
                    TrackName+="-${Soloist[${Track}]}"
                fi
            else
                # track name for single composer/artist CD
                # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
                TrackName="${Albumcomposer}-$(printf "%02u-%02u" ${Discnumber} ${Track})"
                TrackName="-${Title[${Track}]}-${Albumartist}-${Albumconductor}"
                # following items may be blank
                if [[ -n "${Albumconductor}" ]]; then
                    TrackName+="-${Albumconductor}"
                fi
                if [[ -n "${Soloist[${Track}]}" ]]; then
                    TrackName+="-${Soloist[${Track}]}"
                fi
            fi
        else # not 'classical' so assume a popular genre
            if (( VariousArtists == TRUE )); then
                # track name for multiple artist CD
                # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
                TrackName="${Album}-$(printf "%02u-%02u" ${Discnumber} ${Track})"
                TrackName+="-${Artist[${Track}]}-${Title[${Track}]}"
            else
                # track name for single artist CD
                TrackName="${Albumartist}-${Album}"
                # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
                TrackName+="-$(printf "%02u-%02u" ${Discnumber} ${Track})-${Title[${Track}]}"
            fi
        fi
        TrackName="$(fnMunge "${TrackName}").flac"
        # copy and rename
        if cp -a "${FlacFile}" "${TrackDir}${TrackName}"; then
            fnLog "${FlacFile/#${WorkDir}/...} ==> ${TrackDir/#${WorkDir}/...}${TrackName}"
        else
            fnLog "FAIL: ${FlacFile/#${WorkDir}/...} ==> ${TrackDir/#${WorkDir}/...}${TrackName}"
        fi
    done
    if (( Result == TRUE )); then
        fnEcho "renamed flac files using tag values"
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnCopyToLibrary() {
# ------------------------------------------------------------------------------------ #
# Copy the renamed files in 'tracks' to an appropriate directory within the library    #
# folder. Create and directories as required.                                          #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    ArtistDir="${LIBRARY}${Albumartistsort:-${Albumartist}}/"
    declare    AlbumDir="${ArtistDir}${Album}/"
    declare    ComposerDir="${LIBRARY}${Albumcomposersort:-${Albumcomposer}}/"
    declare    ClassicalDir="${ComposerDir}${Album}/"
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    ArtistDir="$(fnMunge "${ArtistDir}")" # munge the artist directory name
    AlbumDir="$(fnMunge "${AlbumDir}")"   # munge the artist name
    AlbumDir="${AlbumDir//\"}"            # delete all quotes
    fnLog "Copy flac files from ${TrackDir} to ${AlbumDir}"
    # is genre in the classical list
    if [[ " ${CLASSICAL[*],,} " =~ \ ${Genre,,}\  ]]; then
        if fnMakeLibraryDir "${ComposerDir}"; then
            if fnMakeLibraryDir "${ClassicalDir}"; then
                for Track in $(${FIND} "${TrackDir}" -name "*.flac" -print | sort); do
                    cp -a "${Track}" "${ClassicalDir}"
                    fnLog "${Track/#${WorkDir}/...} ==> ${ClassicalDir}"
                done
                fnEcho "moved renamed flac files to music library"
            else
                Result=${FALSE}
            fi
        else
            Result=${FALSE}
        fi
    else
        if fnMakeLibraryDir "${ArtistDir}"; then
            if fnMakeLibraryDir "${AlbumDir}"; then
                for Track in $(${FIND} "${TrackDir}" -name "*.flac" -print | sort); do
                    cp -a "${Track}" "${AlbumDir}"
                    fnLog "${Track/#${WorkDir}/...} ==> ${AlbumDir}"
                done
                fnEcho "moved renamed flac files to music library"
            else
                Result=${FALSE}
            fi
        else
            Result=${FALSE}
        fi
    fi
    # shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -ir NOW=-1                      # used for printf %T (time) format descriptor
declare -r  SCRIPTNAME="${0##*/}"       # base name of script (see bash parameter expansion)
# get the script version number from the source header
declare -r  VERSION="$(cut -c17-25 <<< "$(grep -m1 "VERSION:" "${0}")")"
# full paths to executables
declare -r  HTTP="/usr/bin/curl -s"
declare -r  FIND="/usr/bin/find"
declare -r  RIPPER="/usr/bin/cdparanoia"
declare -r  FIXEOL="/usr/bin/dos2unix"
declare -r  CDDISCID="/usr/bin/cd-discid"
declare -r  ENCODE="/usr/bin/flac"
declare -r  TAGGER="/usr/bin/metaflac"
declare -r  COPYART="/usr/bin/magick"
declare -r  ICONV="/usr/bin/iconv"
# path to root of work area for ripping audio files
declare -r  WORK="/home/swillber/Music/Z99_Work/"
# path to music library root
declare -r  LIBRARY="/home/swillber/Music/C10_Library/"
# used to ensure a separate instance of the editor is invoked
declare -r  NEWINSTANCE="--new-instance"
declare -r  GNUDBURL="http://gnudb.gnudb.org:80/~cddb/cddb.cgi"
declare -r  DBUSER="swillber"
declare -r  DBHOST="gmail.com"
declare -ir PROTOCOL=6
declare -r  HELLO="hello=${DBUSER}+${DBHOST}+gnutest+0.1&proto=${PROTOCOL}"
declare -r  FILEPATTERN="track%02u.cdda."  # ensure 2 digit track no., zero prefixed
declare -r  FLACPATTERN="${FILEPATTERN}flac"
# console output controls -----------------------------------------------------------
declare -r  RLF="$(tput cuu 1)"         # reverse line feed
declare -r  CLEOL="$(tput el)"          # clear to end of line
declare -r  RLFCLR="${RLF}${CLEOL}"     # reverse line feed & clear to end of line
# -----------------------------------------------------------------------------------
declare -r  VARIOUS="various"           # string used to identify 'various artists' CDs
# arrays holding vorbis comment keywords
declare -ra VORALBUM=("ALBUMARTISTSORT" "DISCNUMBER" "TOTALDISCS" "TOTALTRACKS")
declare -ra VORTRACK=("COMPOSER" "COMPOSERSORT" "CONDUCTOR")
# arrays holding lists of genres to define metagenres, if its not here then genre
# is assumed to be in the popular music metagenre
declare -ra CLASSICAL=("Classical")
declare -ra SOUNDTRACK=("Soundtrack")
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${FALSE}
declare    Time                         # total time of all tracks
declare -i NTracks
declare    DiscID
declare    WorkDir                      # WORK/(discid)
declare    WavDir                       # WorkDir/(wavfiles)
declare    FlacDir                      # WorkDir/(flacs)
declare    TrackDir                     # WorkDir/(tracks)
declare    Log="${WORK}run.log"         # modified by fnMoveLog
declare    BulkXmcd
declare    XmcdFile
declare    CoverArt
declare    WavFile
declare    FlacFile
declare -a Category                     # array of category matches from fnGetGnuDBEntriesList
declare -a Offset                       # array of offsets saved from cd-discid results
declare -i NoMatchFound=${FALSE}        # set to TRUE if a blank template is required
declare -i VariousArtists=${FALSE}      # set to TRUE if a various artists CD is being processed
# flac tag fields populated from xmcd file
# tags relating to entire album
declare    Album=""
declare    Albumartist=""
declare    Albumartistsort=""
declare    Albumcomposer=""
declare    Albumcomposersort=""
declare    Albumconductor=""
declare    Description=""
declare    Date=""
declare    Genre=""
declare -i Discnumber=0
declare -i Totaldiscs=0
declare -i Totaltracks=0
# tags relating to individual tracks
declare -a Artist
declare -a Composer
declare -a Conductor
declare -a Title
declare -a Soloist
#
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#
fnMain
# shellcheck disable=SC2248 # Prefer double quoting even when variables don't contain special characters.
exit ${Result}
# ------------------------------------------------------------------------------------ #
